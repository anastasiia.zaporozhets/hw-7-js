function filterBy(arr, type) {
    const filteredArr = [];
    arr.forEach(item => {
        if (typeof item !== type) {
            filteredArr.push(item);
        }
    });
    return filteredArr;
}

// function filterByFilter(arr, type) {
//     return arr.filter(item => typeof item !== type);
// }

const arr = ['apple', 'banana', 29, '23', null, true, false, 1994];
const allTypes = ['string', 'number', 'boolean', 'object', 'null', 'undefined'];
allTypes.forEach(type => console.log(filterBy(arr, type)));
// allTypes.forEach(type => console.log(filterByFilter(arr, type)));//другий варінт для filter


